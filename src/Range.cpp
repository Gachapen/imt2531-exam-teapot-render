#include "Range.h"

Range::Range():
	start(0.0),
	end(1.0)
{
}

Range::Range(float start, float end):
	start(start),
	end(end)
{}

float Range::getLength() const
{
	return end - start;
}

float Range::clamp(float value) const
{
	if (value < start) {
		return start;
	} else if (value > end) {
		return end;
	} else {
		return value;
	}
}

