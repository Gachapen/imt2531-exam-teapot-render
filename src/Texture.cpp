#include "Texture.h"

#include <iostream>
#include <SOIL/SOIL.h>

Texture::Texture():
	vao(0),
	textureBuffer(0),
	NUM_VERTICES(4),
	textureLoaded(false),
	width(0),
	height(0)
{}

Texture::Texture(const std::string& file, const glm::vec2& textureOrigin):
	vao(0),
	textureBuffer(0),
	NUM_VERTICES(4),
	textureLoaded(false),
	width(0),
	height(0)
{
	load(file, textureOrigin);
}

Texture::~Texture()
{
	if (textureLoaded == true) {
		freeBuffers();
	}
}

bool Texture::load(const std::string& file, const glm::vec2& textureOrigin)
{
	// Free the buffers if a texture already is loaded.
	if (textureLoaded == true) {
		freeBuffers();
		textureLoaded = false;
	}

	// Load the image.
	int imageChannels;
	unsigned char* imageData = SOIL_load_image(file.c_str(), &width, &height, &imageChannels, SOIL_LOAD_AUTO);

	// Check if it could be loaded.
	if(imageData == nullptr) {
		std::cerr << "Failed to load texture \"" << file << "\": " << SOIL_last_result() << std::endl;
		return false;
	}

	// Create the texture buffer from the loaded image data.
	textureBuffer = SOIL_create_OGL_texture(imageData, width, height, imageChannels, SOIL_CREATE_NEW_ID, 0);
	delete[] imageData;

	// Failed to create texture buffer.
	if(textureBuffer == 0) {
		std::cerr << "Failed to create GL texture \"" << file << "\": " << SOIL_last_result() << std::endl;
		return false;
	}


	GLfloat* vertices = getVerticesFromTextureOrigin(textureOrigin);

	GLfloat textureCoords[] = {
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 1.0f
	};

	// The buffer size is the same for both vertex and tex coord.
	GLsizeiptr bufferSize = sizeof(textureCoords);

	// Generate and bind the vao.
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Get vacant buffer names.
	glGenBuffers(NUM_BUFFERS, buffers);

	// Store the vertices.
	glBindBuffer(GL_ARRAY_BUFFER, buffers[VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, bufferSize, vertices, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glEnableVertexAttribArray(0);

	// Store the texture coordinates
	glBindBuffer(GL_ARRAY_BUFFER, buffers[TEX_COORD]);
	glBufferData(GL_ARRAY_BUFFER, bufferSize, textureCoords, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glEnableVertexAttribArray(1);

	// Restore bindings and delete vertices.
	glBindVertexArray(0);
	delete[] vertices;

	textureLoaded = true;
	return true;
}

GLfloat* Texture::getVerticesFromTextureOrigin(const glm::vec2& textureOrigin)
{
	// Calculate the quad vertices from the origin of the texture (0.0, 0.0).
	GLfloat* vertices = new GLfloat[8];
	vertices[0] = 0.0f - (width * textureOrigin.x);
	vertices[1] = 0.0f - (height * textureOrigin.y);
	vertices[2] = 0.0f - (width * textureOrigin.x);
	vertices[3] = height - (height * textureOrigin.y);
	vertices[4] = width - (width * textureOrigin.x);
	vertices[5] = 0.0f  - (height * textureOrigin.y);
	vertices[6] = width - (width * textureOrigin.x);
	vertices[7] = height  - (height * textureOrigin.y);

	return vertices;
}

void Texture::render() const
{
	if (textureLoaded == false) {
		std::cerr << "E: Trying to render while no texture loaded loaded\n";
		return;
	}

	// Render the texture.
	glBindTexture(GL_TEXTURE_2D, textureBuffer);
	glBindVertexArray(vao);
 	glDrawArrays(GL_TRIANGLE_STRIP, 0, NUM_VERTICES);
 	glBindVertexArray(0);
 	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::freeBuffers()
{
	// Delete all buffers.
	glDeleteTextures(1, &textureBuffer);
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(NUM_BUFFERS, buffers);
}
