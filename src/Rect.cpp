/*
 * Rect.cpp
 *
 *  Created on: Nov 26, 2012
 *      Author: magnus
 */

#include "Rect.h"

Rect::Rect():
	x(0.0),
	y(0.0),
	width(0.0),
	height(0.0)
{
}

Rect::Rect(float x, float y, float width, float height):
	x(x),
	y(y),
	width(width),
	height(height)
{
}

Rect::~Rect()
{
}
