#ifndef EVENTS_H_
#define EVENTS_H_

#include <SDL/SDL_events.h>

/**
 * Inherited to easily handle SDL events.
 */
class Events {
public:
	Events();
	virtual ~Events();

	/**
	 * Send an event to be handled by the class.
	 * @param event The event to be handled.
	 */
	virtual void onEvent(const SDL_Event& event);

protected:
	/**
	 * Window resize event.
	 * @param width Width of the new window size.
	 * @param height Height of the new window size.
	 */
	virtual void onResize(unsigned int width, unsigned int height) {}

	/**
	 * Exit event.
	 */
	virtual void onExit() {}

	/**
	 * Key down event.
	 * @param keysym The key sym of the key pressed.
	 */
	virtual void onKeyDown(const SDL_keysym& keysym) {}

	/**
	 * Key up event.
	 * @param keysym The key sym og the key released.
	 */
	virtual void onKeyUp(const SDL_keysym& keysym) {}

	/**
	 * Mouse motion event.
	 * @param mouseMotion The mouse motion event.
	 */
	virtual void onMouseMotion(const SDL_MouseMotionEvent& mouseMotion) {}

	/**
	 * Mouse down event.
	 * @param mouseButton The mouse button event.
	 */
	virtual void onMousePress(const SDL_MouseButtonEvent& mouseButton) {}

	/**
	 * Mouse release event.
	 * @param mouseButton The mouse button event.
	 */
	virtual void onMouseRelease(const SDL_MouseButtonEvent& mouseButton) {}

	/**
	 * Event not currently supported by any other event callback functions.
	 * @param event The event.
	 */
	virtual void onUnknownEvent(const SDL_Event& event) {}
};

#endif /* EVENTS_H_ */
