#include "Application.h"

int main(int argc, char *argv[])
{
	// Create a new application.
	Application app(800, 600, 32, "Graphics Exam");

	// Run the application loop.
	return app.execute();
}
