#include "Slider.h"

#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

Slider::Slider(float length, Range valueRange, float standardValue, glm::vec2 position, Rect handle, glm::vec3 handleColor):
	length(length),
	valueRange(valueRange),
	value(standardValue),
	position(position),
	handle(handle, handleColor),
	vao(0),
	NUM_VERTICES(2)
{
	// Set the position of the handle to the standard value.
	this->handle.setPosition(getHandlePositionFromValue(value));

	generateModel();
}

Slider::~Slider()
{
	// Delete the buffers.
	if (vao != 0) {
		glDeleteVertexArrays(1, &vao);
		glDeleteBuffers(NUM_BUFFERS, buffers);
	}
}

float Slider::getValue() const
{
	// Clamp the value within the range.
	return valueRange.clamp(value);
}

void Slider::onMousePress(const SDL_MouseButtonEvent& mouseButton)
{
	// If left mouse button is clicked.
	if (mouseButton.button == SDL_BUTTON_LEFT) {
		// Check if the mouse click was within the handle, and set it to clicked if so.
		if (handle.isWithin(glm::vec2(mouseButton.x, mouseButton.y) - position)) {
			handle.setClicked(true);
		}
	}
}

void Slider::onMouseMotion(const SDL_MouseMotionEvent& mouseMotion)
{
	// If the handle is being controlled.
	if (handle.isClicked()) {
		// Update the slider value from the relative mouse movement.
		value += getValueChangeFromRelX(mouseMotion.xrel);

		// Set the position of the handle according to the new value.
		handle.setPosition(getHandlePositionFromValue(value));
	}
}

void Slider::onMouseRelease(const SDL_MouseButtonEvent& mouseButton)
{
	// If the handle is being controlled.
	if (handle.isClicked()) {
		// Set it to not controlled.
		handle.setClicked(false);

		// Clamp the value within the range.
		value = valueRange.clamp(value);
	}
}

void Slider::generateModel()
{
	// The vertices for the slider bar.
	GLfloat lineVertices[] = {
		0.0f, 0.0f,
		length, 0.0f
	};

	// The color for the slider bar.
	GLfloat lineColors[] = {
		0.5, 0.5, 0.5,
		0.5, 0.5, 0.5
	};

	// Generate and bind the vao.
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Generate the buffers.
	glGenBuffers(NUM_BUFFERS, buffers);

	glBindBuffer(GL_ARRAY_BUFFER, buffers[VERTEX_BUFF]); 				   // Bind the vertex buffer.
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineVertices), lineVertices, GL_STATIC_DRAW); // Send the vertex data to the buffer.
	glVertexAttribPointer((GLuint)0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);		   // Set the attribute pointer to the vertex attribute.
	glEnableVertexAttribArray(0);							   // Enable the vertex attribute array.

	glBindBuffer(GL_ARRAY_BUFFER, buffers[COLOR_BUFF]);				// Bind the color buffer.
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineColors), lineColors, GL_STATIC_DRAW);	// Send the color data to the buffer.
	glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);		// Set the attribute pointer to the color attribute.
	glEnableVertexAttribArray(1);							// Enable the color attribute array.

	// Unbind the vao.
	glBindVertexArray(0);
}

void Slider::render(MvpShaderProgram& shader) const
{
	// Translate to the slider position.
	shader.pushTranslation(glm::vec3(position, 0.0f));

	// Update the shader with the new data.
	shader.updateModelViewProjectionMatrix();

	// Draw the slider.
	glBindVertexArray(vao);
	glLineWidth(4.0f);
	glDrawArrays(GL_LINES, 0, NUM_VERTICES);
	glBindVertexArray(0);

	// Draw the handle.
	handle.render(shader);

	// Pop the translation.
	shader.popTransformation();
}

glm::vec2 Slider::getHandlePositionFromValue(float value)
{
	// Clamp the value.
	value = valueRange.clamp(value);

	// The value is translated relative to the range start so that if the value is range.start, it will now be 0.0,
	// then this is scaled to between 0.0-1.0 using the range length, and then scaled to the slider length.
	float xPos = ((value - valueRange.start) / valueRange.getLength()) * length;
	return glm::vec2(xPos, 0.0);
}

bool Slider::isClicked() const
{
	return handle.isClicked();
}

float Slider::getValueChangeFromRelX(int relX)
{
	// Scale the movement from pixels to between 0.0-1.0 using the slider length, then scale to the value range.
	return ((float)relX / length) * valueRange.getLength();
}
