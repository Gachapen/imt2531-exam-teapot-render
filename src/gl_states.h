#ifndef GL_STATES_H_
#define GL_STATES_H_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/gl.h>
#include <stack>
#include "ShaderProgram.h"

/**
 * Light properties. (position and colors).
 */
struct Light {
	glm::vec4 position;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
};

/**
 * Material properties.
 */
struct Material {
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	GLfloat shininess;
};

/**
 * ModelViewProjection matrices.
 */
struct ModelViewProjection {
	std::stack<glm::mat4> modelMatrix;
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;
};

#endif /* GL_STATES_H_ */
