#ifndef CAMERA_H_
#define CAMERA_H_

#include <SDL/SDL.h>
#include <glm/glm.hpp>

/**
 * Class representing a camera view in OpenGL.
 * Used to set the view position for rendering.
 */
class Camera {
public:
	/** Construct a default camera with default values. */
	Camera();

	/**
	 * Construct a camera with specific position.
	 *
	 * @param angleX x-axis angle.
	 * @param angleY y-axis angle.
	 * @param zoom Zoom level.
	 * @param responsiveness The camera movement responsiveness.
	 */
	Camera(float angleX, float angleY, float zoom, float responsiveness);

	virtual ~Camera();

	/**
	 * Changes the camera position according to the mouse motion.
	 * Will only move camera when left mouse button is held.
	 * Zooms if CTRL is also held.
	 *
	 * @param event The mouse motion event.
	 */
	void onMouseMove(const SDL_MouseMotionEvent& event);

	/**
	 * Update the view matrix according to the camera positions.
	 *
	 * @param viewMatrix The view matrix to update.
	 */
	void updateViewMatix(glm::mat4& viewMatrix) const;

private:
	/** The x-axis angle */
	float angleX;

	/** The y-axis angle */
	float angleY;

	/**
	 * The zoom level.
	 * 0 is closest. > 0 is further away.
	 */
	float zoom;

	/** The responsiveness of the camera movement. */
	float responsiveness;
};

#endif /* CAMERA_H_ */
