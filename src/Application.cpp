#include "Application.h"

#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

Application::Application(unsigned int windowWidth, unsigned int windowHeight, unsigned int windowBpp, const std::string& windowTitle):
	RenderApp(windowWidth, windowHeight, windowBpp, windowTitle),
	backgroundTexture(nullptr),
	model(nullptr),
	colorController(nullptr),
	camera(0.0f, 0.0f, 6.0f, 0.2f)
{
}

Application::~Application()
{
	delete backgroundTexture;
	delete model;
	delete colorController;
}

bool Application::initGl()
{
	//Initialize clear color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Initialize the depth buffer.
	glClearDepth(1.0f);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);

	// Enable Smooth Shading.
	glShadeModel(GL_SMOOTH);

	//Really Nice Perspective.
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// Load the phong shader.
	if (phongShader.loadShaderProgram("shader/phongLighting.vert", "shader/phongLighting.frag") == false) {
		return false;
	}

	// Initialize the shader and set the view and projection matrices.
	phongShader.init();

	// Set the light properties.
	light.position = glm::vec4(1.0, 1.0, 0.0, 1.0);
	light.ambient = glm::vec3(1.0, 1.0, 1.0);
	light.diffuse = glm::vec3(1.0, 1.0, 1.0);
	light.specular = glm::vec3(1.0, 1.0, 1.0);

	// Set the material properties.
	material.ambient = glm::vec3(1.0, 1.0, 1.0) * 0.2f;
	material.diffuse = glm::vec3(1.0, 1.0, 1.0) * 0.6f;
	material.specular = glm::vec3(1.0, 1.0, 1.0);
	material.shininess = 100.0f;

	// Load and initialize the ortho shader..
	if (orthoShader.loadShaderProgram("shader/gui.vert", "shader/gui.frag") == false) {
		return false;
	}
	orthoShader.init();

	// Load and initialize the texture shader..
	if (textureShader.loadShaderProgram("shader/ortho_texture.vert", "shader/ortho_texture.frag") == false) {
		return false;
	}
	textureShader.init();

	// Load the texture, model, and create the color controller.
	backgroundTexture = new Texture("img/background.jpg");
	model = new Object("model/teapot.txt");
	colorController = new ColorController(700.0f, Range(0.0f, 1.0f), Rect(-7.5f, -20.0f, 15.0f, 40.0f), glm::vec2(30.0f, 50.0f), 10.0f);

	// Send a resize event (sets the matrices).
	onResize(getWindowWidth(), getWindowHeight());

	return true;
}

void Application::update()
{
	// Update the color according to the controller.
	glm::vec3 color = colorController->getColor();
	material.ambient = color * 0.2f;
	material.diffuse = color * 0.6f;
}

void Application::render()
{
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	// Ready background rendering.
	glDisable(GL_DEPTH_TEST);
	textureShader.enable();

	// Translate the texture to the center of the screen and update the shader's mvpm.
	textureShader.pushTranslation(glm::vec3(getWindowWidth() / 2.0, getWindowHeight() / 2.0, 0.0));
	textureShader.updateModelViewProjectionMatrix();

	// Set the active texture and render it.
	glActiveTexture(GL_TEXTURE0);
	backgroundTexture->render();

	// Pop of the translation.
	textureShader.popTransformation();

	// Ready the 3D rendering.
	glEnable(GL_DEPTH_TEST);
	phongShader.enable();

	// Update the view matrix from the camera.
	glm::mat4 cameraViewMatrix;
	camera.updateViewMatix(cameraViewMatrix);
	phongShader.setViewMatrix(cameraViewMatrix);

	// Move the model down.
	phongShader.pushTranslation(glm::vec3(0.0f, -1.5, 0.0f));

	// Update the shader matrices.
	phongShader.updateModelViewMatrix();
	phongShader.updateModelViewProjectionMatrix();
	phongShader.updateNormalMatrix();

	// Update the light properties in the shader.
	glUniform4fv(phongShader.getUniformLocation("light.position"), 1, &light.position[0]);
	glUniform3fv(phongShader.getUniformLocation("light.ambient"), 1, &light.ambient[0]);
	glUniform3fv(phongShader.getUniformLocation("light.diffuse"), 1, &light.diffuse[0]);
	glUniform3fv(phongShader.getUniformLocation("light.specular"), 1, &light.specular[0]);

	// Update the material properties in the shader.
	glUniform3fv(phongShader.getUniformLocation("material.ambient"), 1, &material.ambient[0]);
	glUniform3fv(phongShader.getUniformLocation("material.diffuse"), 1, &material.diffuse[0]);
	glUniform3fv(phongShader.getUniformLocation("material.specular"), 1, &material.specular[0]);
	glUniform1f(phongShader.getUniformLocation("material.shininess"), material.shininess);

	// Render the model.
	model->draw();

	// Pop the model translation.
	phongShader.popTransformation();

	// Ready the controller rendering.
	glDisable(GL_DEPTH_TEST);
	orthoShader.enable();

	// Render the controller.
	colorController->render(orthoShader);

	// Swap the buffers.
	SDL_GL_SwapBuffers();
}

void Application::onEvent(const SDL_Event& event)
{
	Events::onEvent(event);
	colorController->onEvent(event);
}


void Application::onResize(unsigned int width, unsigned int height)
{
	// Resize the app and set the viewport.
	RenderApp::onResize(width, height);
	glViewport(0, 0, width, height);

	// Update projection matrices.
	phongShader.setProjectionMatrix(glm::perspective(60.0f, float(getWindowWidth()) / float(getWindowHeight()), 0.1f, 1000.0f));
	orthoShader.setProjectionMatrix(glm::ortho(0.0f, float(width), float(height), 0.0f));
	textureShader.setProjectionMatrix(glm::ortho(0.0f, float(width), float(height), 0.0f));
}

void Application::onKeyDown(const SDL_keysym& keysym)
{
	switch (keysym.sym) {
	case SDLK_ESCAPE:
		onExit();
		break;

	default:
		break;
	}
}

void Application::onMouseMotion(const SDL_MouseMotionEvent& mouseMotion)
{
	// Control the camera if not controlling the colors.
	if (colorController->isClicked() == false) {
		camera.onMouseMove(mouseMotion);
	}
}
