#include "Object.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <glm/glm.hpp>

using namespace std;

Object::Object() :
    mHasNormals(false),
    mHasTexCoords(false),
    mTexID(0)
{
    glGenBuffers(BUFFER_COUNT, mBuffers); //Create the Vertex Buffer Objects (VBO's)
}

Object::Object(const string &filename) :
    mHasNormals(false),
    mHasTexCoords(false),
    mTexID(0)
{
    glGenBuffers(BUFFER_COUNT, mBuffers);  //Create the Vertex Buffer Objects (VBO's)
    loadTxtFile(filename);
}

Object::~Object()
{
    glDeleteBuffers(BUFFER_COUNT, mBuffers);
    if(mTexID != 0) {
        glDeleteTextures(1, &mTexID);
    }
}

bool Object::loadTxtFile(const string &filename)
{
    vector<glm::vec3> vertices;
    vector<glm::vec3> faceNormals;
    vector<GLuint> indices;
    vector<glm::vec3> verticesDuplicated;
    vector<glm::vec3> normalsDuplicated;
    
    fstream in(filename);
    if(!in.good()) {
        cout << "Error opening file " << filename << endl;
        return false;
    }

    size_t numVertices;
    size_t numTris;

    in >> numVertices >> numTris;

    GLfloat x, y, z;
    for(size_t j=0; j<numVertices; ++j) {
        in >> x >> y >> z;

        vertices.push_back(glm::vec3(x, y, z));
    }
    GLuint a, b, c;
    for(size_t j=0; j<numTris; ++j) {
        in >> a >> b >> c;
        indices.push_back(a-1);
        indices.push_back(b-1);
        indices.push_back(c-1);

        glm::vec3 A = vertices[a-1];
        glm::vec3 B = vertices[b-1];
        glm::vec3 C = vertices[c-1];
        glm::vec3 normal = glm::normalize(glm::cross(B - A, C - A));
        faceNormals.push_back(normal);

        verticesDuplicated.push_back(A);
        verticesDuplicated.push_back(B);
        verticesDuplicated.push_back(C);
        normalsDuplicated.push_back(normal);
        normalsDuplicated.push_back(normal);
        normalsDuplicated.push_back(normal);
    }

    cout << "vertices = " << vertices.size() << endl;
    cout << "indices  = " << indices.size()/3 << endl;

    if(!GLEW_VERSION_1_5) {
        cout << "Error: Vertex buffer objects (VBO's) are not supported" << endl;
        exit(1);
    }

    vector<glm::vec2> texCoords;
    updateVbo(verticesDuplicated, normalsDuplicated, texCoords);

    return true;
}

// Update the vertex buffer objects
bool Object::updateVbo(const vector<glm::vec3> &vertices, const vector<glm::vec3> &normals, const vector<glm::vec2> &texCoords)
{
    if(!GLEW_VERSION_1_5) {
        cout << "Error: Vertex buffer objects (VBO's) are not supported" << endl;
        return false;
    }

    //Bind buffer index 0 (thus making it the active ARRAY_BUFFER)
    glBindBuffer(GL_ARRAY_BUFFER, mBuffers[VERTEX]);
    //Put the vextex data into the active/bound buffer (buffer 0)
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(glm::vec3), &vertices.front(), GL_STATIC_DRAW);
    mElementCount = vertices.size();

    mHasNormals = !normals.empty();
    if(mHasNormals) {
        //Put normals data into buffer 2 (by binding buffer 2 then specifying the normals data)
        glBindBuffer(GL_ARRAY_BUFFER, mBuffers[NORMAL]);
        glBufferData(GL_ARRAY_BUFFER, normals.size()*sizeof(glm::vec3), &normals.front(), GL_STATIC_DRAW);
    }

    mHasTexCoords = !texCoords.empty();
    if(mHasTexCoords) {
        //Put normals data into buffer 2 (by binding buffer 2 then specifying the normals data)
        glBindBuffer(GL_ARRAY_BUFFER, mBuffers[TEX_COORDS]);
        glBufferData(GL_ARRAY_BUFFER, texCoords.size()*sizeof(glm::vec2), &texCoords.front(), GL_STATIC_DRAW);
    }
    return true;
}

void Object::draw()
{
    //Bind the buffers and tell OpenGL to use the Vertex Buffer Objects (VBO's), which we already prepared earlier
    glBindBuffer(GL_ARRAY_BUFFER, mBuffers[VERTEX]);
    //For the vertex we are using attribute index 0
    glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, nullptr); // Set up our vertex attributes pointer
    glEnableVertexAttribArray(0);

    if(mHasNormals) {
        glBindBuffer(GL_ARRAY_BUFFER, mBuffers[NORMAL]);
        //For the normal we are using attribute index 1
        glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, nullptr); // Set up our vertex attributes pointer
        glEnableVertexAttribArray(1);
    }
    if(mHasTexCoords) {
        glBindBuffer(GL_ARRAY_BUFFER, mBuffers[TEX_COORDS]);
        //For the texCoords we are using attribute index 2
        glVertexAttribPointer((GLuint)2, 2, GL_FLOAT, GL_FALSE, 0, nullptr); // Set up our vertex attributes pointer
        glEnableVertexAttribArray(2);
    }

    //Draw the thing!
    glDrawArrays(GL_TRIANGLES, 0, mElementCount);

    //restore the GL state back
    if(mHasTexCoords) {
        glDisableVertexAttribArray(2);
    }
    if(mHasNormals) {
        glDisableVertexAttribArray(1);
    }
    glDisableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0); //Restore non VBO mode
}
