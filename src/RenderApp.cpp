#include "RenderApp.h"

#include <iostream>

RenderApp::RenderApp(unsigned int windowWidth, unsigned int windowHeight, unsigned int windowBpp, const std::string& windowTitle):
	running(false),
	windowWidth(windowWidth),
	windowHeight(windowHeight),
	bpp(windowBpp),
	windowTitle(windowTitle),
	window(nullptr)
{
}

RenderApp::~RenderApp()
{
}

int RenderApp::execute()
{
	// Initialize everything.
	if (init() == false) {
		return false;
	}

	// Loop as long as the app is active.
	while (running) {
		handleEvents();
		update();
		render();
	}

	// Clean things up.
	cleanup();

	return 0;
}

bool RenderApp::init()
{
	if (initSdl() == false) {
		return false;
	}

	if (initGlew() == false) {
		return false;
	}

	if (initGl() == false) {
		return false;
	}

	// Ready to start app loop.
	running = true;
	return true;
}

bool RenderApp::initSdl()
{
	//Initialize SDL.
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		std::cout << "Error calling SDL_Init: " << SDL_GetError() << std::endl;
		return false;
	}

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); //Enable double buffering.
	SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 1); //Turn on VSync.

	// Multisampling (removing jaggy edges).
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);

	// Create the SDL Window to render OpenGL stuff on.
	window = SDL_SetVideoMode(windowWidth, windowHeight, bpp, SDL_OPENGL | SDL_RESIZABLE);
	if (window == nullptr) {
		std::cout << "Couldn't set SDL window mode: " << SDL_GetError() << std::endl;
		return false;
	}

	// Set the window title.
	SDL_WM_SetCaption( windowTitle.c_str(), NULL );

	//Enable unicode (so we can get unicode keypresses).
	SDL_EnableUNICODE(SDL_TRUE);

	// Successful init.
	return true;
}

bool RenderApp::initGlew()
{
	//Initialise the Glew extension library (this has the be done when we have a GL context).
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		// Problem: glewInit failed, something is seriously wrong.
		std::cout << "Error: %s\n" << glewGetErrorString(err) << std::endl;
		return false;
	}

	// Show some information about the OpenGL verion and graphics card (for debugging)
	std::cout << ::glGetString(GL_VENDOR) << std::endl;
	std::cout << ::glGetString(GL_RENDERER) << std::endl;
	std::cout << ::glGetString(GL_VERSION) << std::endl;

	return true;
}

void RenderApp::cleanup()
{
	// Frees the SDL stuff.
	SDL_Quit();
}

void RenderApp::handleEvents()
{
	SDL_Event event;

	// Poll and handle all the events queued by SDL.
	while (SDL_PollEvent(&event)) {
		onEvent(event);
	}
}

void RenderApp::onResize(unsigned int width, unsigned int height)
{
	// Set the new window dimensions.
	windowWidth = width;
	windowHeight = height;

	// Update the SDL window.
	window = SDL_SetVideoMode(width, height, bpp, SDL_OPENGL | SDL_RESIZABLE);
	if (window == nullptr) {
		std::cout << "Couldn't resize SDL window: " << SDL_GetError() << std::endl;
	}
}

unsigned int RenderApp::getWindowHeight()
{
	return windowHeight;
}

unsigned int RenderApp::getWindowWidth()
{
	return windowWidth;
}

void RenderApp::onExit()
{
	running = false;
}
