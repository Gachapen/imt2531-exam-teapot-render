#ifndef CONTROLL_H_
#define CONTROLL_H_

#include <string>
#include "Slider.h"

/**
 * A controller class which can contain different types of controls.
 * Currently only supports sliders.
 */
class Controller {
public:
	Controller();
	virtual ~Controller();

	/**
	 * Add a slider to the controller.
	 * @param name The name of the slider. Used to refer to it later.
	 * @param slider Pointer to the slider to add. This will be deleted at this object's destruction.
	 */
	void addSlider(const std::string& name, Slider* slider);

	/**
	 * Get the value of a slider.
	 * @param sliderName The name of the slider.
	 * @return The value of the specified slider.
	 */
	float getSliderValue(const std::string& sliderName);

	/**
	 * Render the controller with all its controls.
	 * @param shader The shader object to render to.
	 */
	void render(MvpShaderProgram& shader) const;

	/**
	 * Handle an event.
	 * Called for each event to check if the mouse uses the controls.
	 * @param event The event to handle.
	 */
	void onEvent(const SDL_Event& event);

protected:
	/** All the slider pointers. */
	std::map<const std::string, Slider*> sliders;
};

#endif /* CONTROLL_H_ */
