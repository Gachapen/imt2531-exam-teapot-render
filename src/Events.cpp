#include "Events.h"

Events::Events()
{
}

Events::~Events()
{
}

void Events::onEvent(const SDL_Event& event)
{
	// Check what type of event it is and call the correct function.
	switch (event.type) {
	case SDL_VIDEORESIZE:
		onResize(event.resize.w, event.resize.h);
		break;

	case SDL_QUIT:
		onExit();
		break;

	case SDL_KEYDOWN:
		onKeyDown(event.key.keysym);
		break;

	case SDL_KEYUP:
		onKeyUp(event.key.keysym);
		break;

	case SDL_MOUSEMOTION:
		onMouseMotion(event.motion);
		break;

	case SDL_MOUSEBUTTONDOWN:
		onMousePress(event.button);
		break;

	case SDL_MOUSEBUTTONUP:
		onMouseRelease(event.button);
		break;

	// An event which's type is not currently handled.
	default:
		onUnknownEvent(event);
		break;
	}
}
