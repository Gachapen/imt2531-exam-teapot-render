#ifndef RENDERAPP_H_
#define RENDERAPP_H_

#include <SDL/SDL.h>
#include <GL/glew.h>
#include <string>
#include "Events.h"

/**
 * A virtual class to inherit from when creating an app for rendering width SDL/OpenGL.
 */
class RenderApp: public Events {
public:
	/**
	 * Create a new render app.
	 * @param windowWidth Height of the window to draw on.
	 * @param windowHeight Width of the window to draw on.
	 * @param windowBpp Bits per pixel.
	 * @param windowTitle Title of the window to draw on.
	 */
	RenderApp(unsigned int windowWidth, unsigned int windowHeight, unsigned int windowBpp, const std::string& windowTitle);

	virtual ~RenderApp();

	/**
	 * Execute the app's loop.
	 * @return Exit code. 0 -> success. -1 -> error.
	 */
	int execute();

private:
	/** If the app is running or not. Will exit when false. */
	bool running;

	/** Width of the window. */
	unsigned int windowWidth;

	/** Height of the window. */
	unsigned int windowHeight;

	/** Bits per pixel. */
	unsigned int bpp;

	/** Title of the window. */
	std::string windowTitle;

	/** Pointer to the window surface. */
	SDL_Surface* window;

	/**
	 * Initialize the window (SDL -> GLEW -> GL).
	 * @return true if success, otherwise false.
	 */
	bool init();

	/**
	 * Initialize SDL.
	 * @return true if success, otherwise false.
	 */
	bool initSdl();

	/**
	 * Initialize GLEW.
	 * @return true if success, otherwise false.
	 */
	bool initGlew();

protected:
	/**
	 * Initialize OpenGL.
	 * Inheriting classes must implement this.
	 * @return true if success, otherwise false.
	 */
	virtual bool initGl() = 0;

	/**
	 * Clean up before exiting.
	 */
	virtual void cleanup();

	/**
	 * Handle all events queued.
	 */
	virtual void handleEvents();

	/**
	 * Update everything.
	 * Inheriting classes must implement this.
	 */
	virtual void update() = 0;

	/**
	 * Render everything.
	 * Inheriting classes must implement this.
	 */
	virtual void render() = 0;

	/**
	 * Exits the app.
	 */
	void onExit();

	/**
	 * @return The current window width.
	 */
	unsigned int getWindowWidth();

	/**
	 * @return The curren window height.
	 */
	unsigned int getWindowHeight();

protected:
	/**
	 * Window resize handling.
	 * @param width New window width.
	 * @param height New window height.
	 */
	virtual void onResize(unsigned int width, unsigned int height);
};

#endif /* RENDERAPP_H_ */
