#ifndef COLORCONTROL_H_
#define COLORCONTROL_H_

#include <vector>
#include "Controller.h"

/**
 * A controller made specifically for controlling RGB colors.
 */
class ColorController: public Controller {
public:
	/**
	 * Construct a new color controller.
	 * @param length The length of the sliders within this controller.
	 * @param valueRange The range of the values of the sliders within this controller.
	 * @param handleRect The rectangle representing a slider handle within this controller.
	 * @param position The position of this controller.
	 * @param sliderGap The gap between the sliders.
	 */
	ColorController(float length, Range valueRange, Rect handleRect, glm::vec2 position, float sliderGap);

	virtual ~ColorController();

	/**
	 * Get the current controller color.
	 * @return The current controller color.
	 */
	const glm::vec3 getColor();

	/**
	 * Check if the controller is currently clicked (being controlled by the user).
	 * @return true if under control, otherwise false.
	 */
	bool isClicked() const;
};

#endif /* COLORCONTROL_H_ */
