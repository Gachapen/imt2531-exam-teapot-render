#ifndef SHADERPROGRAM_H_
#define SHADERPROGRAM_H_

#include <GL/glew.h>
#include <map>
#include <string>

/**
 * Representing an OpenGL shader program.
 */
class ShaderProgram {
public:
	ShaderProgram();
	virtual ~ShaderProgram();

	/**
	 * Create a shader program from a vertex and fragment shader.
	 * @param vertexShader Path to the vertex shader to compile and link.
	 * @param fragmentShader Path to the fragment shader to compile and link.
	 * @return true if successful, otherwise false.
	 */
	bool loadShaderProgram(const std::string& vertexShader, const std::string& fragmentShader);

	/**
	 * Initialize a uniform variable.
	 * @param name The name of the uniform.
	 * @return true if successful, otherwise false.
	 */
	bool initUniform(const std::string& name);

	/**
	 * Get the location (ID) of the uniform with specified name.
	 * Automatically tries to initialize the uniform location (initUniform) if it is not found in
	 * the uniform map member.
	 *
	 * @param name The name of the uniform to get the location for.
	 * @return The location (ID) of the uniform. -1 if not found in the shader.
	 */
	GLint getUniformLocation(const std::string& name);

	/**
	 * Enable the shader (glUseProgram).
	 */
	void enable();

private:
	/** The shader program ID. */
	GLuint shaderProgram;

	/** The vertex shader ID. */
	GLuint vertexShader;

	/** The fragment shader ID. */
	GLuint fragmentShader;

	/** Map containing all the uniform locations. */
	std::map<std::string, GLuint> uniforms;

	/**
	 * Load a vertex/fragment shader.
	 * @param shaderPath Path to the shader.
	 * @param shaderType GL_VERTEX_SHADER or GL_FRAGMENT_SHADER.
	 * @return The ID of the shader compiled. 0 if failed.
	 */
	GLuint loadShader(const std::string& shaderPath, GLenum shaderType);

	/**
	 * Check if the shader was linked successfully.
	 * @return true if success, otherwise false.
	 */
	bool checkLinkStatus();

	/**
	 * Check if a shader was successfully compiled.
	 * @param shaderId The ID of the shader to check.
	 * @param shaderPath The path to the shader loaded.
	 * @return true if success, otherwise false.
	 */
	bool checkCompileStatus(GLuint shaderId, const std::string& shaderPath);
};

#endif /* SHADERPROGRAM_H_ */
