#ifndef MVPSHADERPROGRAM_H_
#define MVPSHADERPROGRAM_H_

#include "ShaderProgram.h"
#include "gl_states.h"

/**
 * A shader program class specifically designed for shaders using
 * model, view, projection, and/or normal matrices.
 */
class MvpShaderProgram: public ShaderProgram {
public:
	MvpShaderProgram();
	virtual ~MvpShaderProgram();

	/**
	 * Initialize the shader.
	 */
	void init();

	/**
	 * Set the projection matrix.
	 * @param projection The projection matrix.
	 */
	void setProjectionMatrix(const glm::mat4& projection);

	/**
	 * Set the view matrix.
	 * @param view The view matrix.
	 */
	void setViewMatrix(const glm::mat4& view);

	/**
	 * Push a translation.
	 * @param trans Translation to do.
	 */
	void pushTranslation(const glm::vec3& trans);

	/**
	 * Pop the last numPops transformations.
	 * @param numPops Number of transformations to pop. Default = 1.
	 */
	void popTransformation(unsigned int numPops = 1);

	/**
	 * Update the modelViewProjectionMatrix and pass it to the shader.
	 */
	void updateModelViewProjectionMatrix();

	/**
	 * Update the modelViewMatrix and pass it to the shader.
	 */
	void updateModelViewMatrix();

	/**
	 * Pass the normalMatrix to the shader.
	 */
	void updateNormalMatrix();

private:
	/** The matrices. */
	ModelViewProjection mvpm;
};

#endif /* MVPSHADERPROGRAM_H_ */
