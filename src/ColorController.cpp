#include "ColorController.h"

ColorController::ColorController(float length, Range valueRange, Rect handleRect, glm::vec2 position, float sliderGap)
{
	// Gap between the handles.
	sliderGap += handleRect.height;

	// Names of the three sliders.
	std::string names[] = { "red", "green", "blue" };

	// Create the RGB sliders.
	for (unsigned int i = 0; i < 3; i++) {

		// First red, then green, then blue.
		glm::vec3 color(0.0f);
		color[i] = 1.0f;

		// The position of the slider.
		// TODO: Relative to the controller?
		glm::vec2 sliderPosition(position.x, position.y + sliderGap * i);

		// Create a new slider.
		addSlider(names[i], new Slider(length,
					        valueRange,
					        valueRange.end,
					        sliderPosition,
					        handleRect,
					        color)
			 );
	}
}

ColorController::~ColorController()
{
}

const glm::vec3 ColorController::getColor()
{
	// Return the RGB color.
	return glm::vec3(sliders["red"]->getValue(), sliders["green"]->getValue(), sliders["blue"]->getValue());
}

bool ColorController::isClicked() const
{
	// Loop through all sliders and see if one is currently being controlled.
	for (auto sliderIt = sliders.begin(); sliderIt != sliders.end(); sliderIt++) {
		if ((*sliderIt).second->isClicked()) {
			return true;
		}
	}

	// Non being controlled.
	return false;
}
