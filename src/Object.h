#pragma once

#include <GL/glew.h>
#include <GL/gl.h>
#include <string>
#include <vector>
#include <glm/glm.hpp>

using namespace std;

class Object
{
    enum BufferType { VERTEX, NORMAL, TEX_COORDS, BUFFER_COUNT }; //BUFFER_COUNT should aways be at the end
    GLuint mBuffers[BUFFER_COUNT];
    GLuint mElementCount;
    bool mHasNormals;
    bool mHasTexCoords;
    GLuint mTexID;
public:
    Object();
    Object(const string &filename);
    ~Object();

    bool loadTxtFile(const string &filename);
    bool updateVbo(const vector<glm::vec3> &vertices, const vector<glm::vec3> &normals, const vector<glm::vec2> &texCoords);
    void draw();

    bool hasNormals() const   { return mHasNormals; }
    bool hasTexCoords() const { return mHasTexCoords; }
    GLuint getTexID() const   { return mTexID; }
};
