#include "MvpShaderProgram.h"

MvpShaderProgram::MvpShaderProgram()
{
}

MvpShaderProgram::~MvpShaderProgram()
{
}

void MvpShaderProgram::pushTranslation(const glm::vec3& trans)
{
	glm::mat4 translation = glm::translate(mvpm.modelMatrix.top(), trans);
	mvpm.modelMatrix.push(translation);
}

void MvpShaderProgram::popTransformation(unsigned int numPops)
{
	for (unsigned int i = 0; i < numPops; i++) {
		mvpm.modelMatrix.pop();
	}
}

void MvpShaderProgram::init()
{
	if (mvpm.modelMatrix.empty()) {
		mvpm.modelMatrix.push(glm::mat4(1.0f));
	}

	mvpm.viewMatrix = glm::mat4(1.0f);
}

void MvpShaderProgram::setProjectionMatrix(const glm::mat4& projection)
{
	mvpm.projectionMatrix = projection;
}

void MvpShaderProgram::setViewMatrix(const glm::mat4& view)
{
	mvpm.viewMatrix = view;
}

void MvpShaderProgram::updateModelViewProjectionMatrix()
{
	glm::mat4 modelViewProjectionMatrix = mvpm.projectionMatrix * mvpm.viewMatrix * mvpm.modelMatrix.top();
	glUniformMatrix4fv(getUniformLocation("modelViewProjectionMatrix"), 1, GL_FALSE, &modelViewProjectionMatrix[0][0]);
}

void MvpShaderProgram::updateModelViewMatrix()
{
	glm::mat4 modelViewMatrix = mvpm.viewMatrix * mvpm.modelMatrix.top();
	glUniformMatrix4fv(getUniformLocation("modelViewMatrix"), 1, GL_FALSE, &modelViewMatrix[0][0]);
}

void MvpShaderProgram::updateNormalMatrix()
{
	glm::mat4 modelViewMatrix = mvpm.viewMatrix * mvpm.modelMatrix.top();
	glm::mat3 normalMatrix(glm::transpose(glm::inverse(modelViewMatrix)));
	glUniformMatrix3fv(getUniformLocation("normalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);
}
