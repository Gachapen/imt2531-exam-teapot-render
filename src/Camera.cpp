#include "Camera.h"
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>

Camera::Camera():
	angleX(0.0f),
	angleY(0.0f),
	zoom(8.0f),
	responsiveness(0.2f)
{
}

Camera::Camera(float angleX, float angleY, float zoom, float responsiveness):
	angleX(angleX),
	angleY(angleY),
	zoom(zoom),
	responsiveness(responsiveness)
{
}

Camera::~Camera()
{
}

void Camera::onMouseMove(const SDL_MouseMotionEvent& motionEvent)
{
	// If mouse is not pressed, return.
	if (!(motionEvent.state & SDL_BUTTON_LMASK)) {
		return;
	}

	// If CTRL is pressed.
	if (SDL_GetModState() & KMOD_CTRL) {
		// Calculate the zoom value.
		zoom *= static_cast<float>(exp((responsiveness / 100.0) * motionEvent.yrel));
	} else {
		// Calculate the angles.
		angleX += static_cast<float>(motionEvent.xrel) * responsiveness;
		angleY += static_cast<float>(motionEvent.yrel) * responsiveness;
	}
}

void Camera::updateViewMatix(glm::mat4& viewMatrix) const
{
	// Calculate current camera view
	viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -zoom));
	viewMatrix = glm::rotate(viewMatrix, angleY, glm::vec3(1.0f, 0.0f, 0.0f));
	viewMatrix = glm::rotate(viewMatrix, angleX, glm::vec3(0.0f, 1.0f, 0.0f));
}
