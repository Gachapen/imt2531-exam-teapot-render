#ifndef RANGE_H_
#define RANGE_H_

/**
 * A range between two values.
 */
class Range {
public:
	/** The start of the range. */
	float start;

	/** The end of the range. */
	float end;

	/** Construct a default range from 0.0 to 1.0. */
	Range();

	/**
	 * Construct a range with specified values.
	 * @param start Range start.
	 * @param end Range end.
	 */
	Range(float start, float end);

	/**
	 * Get the length of the range.
	 * Length = end - start.
	 * @return The length of the range.
	 */
	float getLength() const;

	/**
	 * Clamp a value within this range.
	 * @param value The value to clamp.
	 * @return The clamped value.
	 */
	float clamp(float value) const;
};

#endif /* RANGE_H_ */
