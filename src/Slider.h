#ifndef SLIDER_H_
#define SLIDER_H_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include "Button.h"
#include "Range.h"
#include "Events.h"

/**
 * Control a value with a slider.
 */
class Slider: public Events {
public:
	/**
	 * Construct a new slider.
	 * @param length The physical length of the slider.
	 * @param valueRange The range of the slider values.
	 * @param standardValue The standard value set when creating the slider.
	 * @param position The position of the slider.
	 * @param handle Rect representing the slider handle.
	 * @param handleColor Color of the handle.
	 */
	Slider(float length, Range valueRange, float standardValue, glm::vec2 position, Rect handle, glm::vec3 handleColor);
	virtual ~Slider();

	/**
	 * Render the slider.
	 * @param shader The shader to render it to.
	 */
	void render(MvpShaderProgram& shader) const;

	/**
	 * Check if the mouse press is on a slider handle.
	 * @param mouseButton Mouse press to check.
	 */
	void onMousePress(const SDL_MouseButtonEvent& mouseButton);

	/**
	 * Check if the slider should be moved.
	 * @param mouseMotion The mouse motion to check.
	 */
	void onMouseMotion(const SDL_MouseMotionEvent& mouseMotion);

	/**
	 * Check if the slider shouldn't be controlled anymore.
	 * @param mouseButton The mouse release to check.
	 */
	void onMouseRelease(const SDL_MouseButtonEvent& mouseButton);

	/**
	 * Get the current value of the slider.
	 * @return The slider's value.
	 */
	float getValue() const;

	/**
	 * Check if the slider is currently being controlled.
	 * @return true if it is controlled, otherwise false.
	 */
	bool isClicked() const;

private:
	/** The physical length of the slider. */
	float length;

	/** The range of the values. */
	Range valueRange;

	/** The current slider value. */
	float value;

	/** The position of the slider. */
	glm::vec2 position;

	/** The slider handle. */
	Button handle;

	/** The types of VBO. */
	enum { VERTEX_BUFF, COLOR_BUFF, NUM_BUFFERS };

	/** The VAO. */
	GLuint vao;

	/** The buffers. */
	GLuint buffers[NUM_BUFFERS];

	/** Number of vertices to render. */
	const int NUM_VERTICES;

	/** Generate the model and put it in the buffer. */
	void generateModel();

	/**
	 * Get the position of the handle from the value specified.
	 * @param value The value to get the position from.
	 * @return The position of the handle.
	 */
	glm::vec2 getHandlePositionFromValue(float value);

	/**
	 * Get the relative change of value from the relative physical handle position change.
	 * @param relX The relative position change.
	 * @return The relative value change.
	 */
	float getValueChangeFromRelX(int relX);
};

#endif /* SLIDER_H_ */
