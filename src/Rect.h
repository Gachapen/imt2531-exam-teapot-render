#ifndef RECT_H_
#define RECT_H_

/** Representing a rectangle. */
class Rect {
public:
	/** X position. (Left side of the rectangle) */
	float x;

	/** Y position. (bottom of the rectangle) */
	float y;

	/** Width of the rectangle. */
	float width;

	/** Height of the rectangle. */
	float height;

	/**
	 * Sets all values to their defaults (0.0).
	 */
	Rect();

	/**
	 * Constructs a rect with specified values.
	 * @param x The x position of the rect.
	 * @param y The y position of the rect.
	 * @param width The width of the rect.
	 * @param height The height of the rect.
	 */
	Rect(float x, float y, float width, float height);

	virtual ~Rect();
};

#endif /* RECT_H_ */
