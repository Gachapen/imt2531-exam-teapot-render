#ifndef SLIDERHANDLE_H_
#define SLIDERHANDLE_H_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include "Rect.h"
#include "MvpShaderProgram.h"

/**
 * A button that can be clicked.
 */
class Button {
public:
	/**
	 * Construct a new button.
	 * @param buttonRect Rectangle representing the button position and dimensions.
	 * @param color The color of the button.
	 */
	Button(const Rect& buttonRect, const glm::vec3& color);

	virtual ~Button();

	/**
	 * Render the button.
	 * @param shader Program to render to.
	 */
	void render(MvpShaderProgram& shader) const;

	/**
	 * Set the position of the button.
	 * @param position The new position.
	 */
	void setPosition(const glm::vec2& position);

	/**
	 * Check if a position is within the button.
	 * Used to check if e.g. the button is clicked by the mouse.
	 * @param position The position to check.
	 * @return true if the position is within the button, otherwise false.
	 */
	bool isWithin(const glm::vec2& position);

	/**
	 * Check if the button is clicked or not.
	 * @return true if clicked, false if not.
	 */
	bool isClicked() const;

	/**
	 * Set the button click state.
	 * @param state Clicked or not.
	 */
	void setClicked(bool state);

private:
	/** Rectangle representing the button. */
	Rect buttonRect;

	/** Position of the button. */
	glm::vec2 position;

	/** Clicked or not. */
	bool clicked;

	/** The types of vbo. */
	enum { VERTEX_BUFF, COLOR_BUFF, NUM_BUFFERS };

	/** The vbos. */
	GLuint buffers[NUM_BUFFERS];

	/** The vao. */
	GLuint vao;

	/** Number of vertices to render. */
	const int NUM_VERTICES;

	/**
	 * Generate the button model.
	 * @param color The color of the model.
	 */
	void generateModel(const glm::vec3& color);
};

#endif /* SLIDERHANDLE_H_ */
