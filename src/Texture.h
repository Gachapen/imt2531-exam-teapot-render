#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>

/**
 * A 2D OpenGL texture.
 */
class Texture {
public:
	/** Constructo object with no loaded texture. */
	Texture();

	/**
	 * Constructo object and load specified texture.
	 * @param fileName Path to the texture file.
	 * @param textureOrigin Where the texture's origin will be. (used like texture coords).
	 */
	Texture(const std::string& fileName, const glm::vec2& textureOrigin = glm::vec2(0.5, 0.5));

	virtual ~Texture();

	/**
	 * Load a texture from file.
	 * @param file Path to the image to load.
	 * @param textureOrigin Where the texture's origin will be. (used like texture coords).
	 * @return true if successfully loaded, otherwise false.
	 */
	bool load(const std::string& file, const glm::vec2& textureOrigin = glm::vec2(0.5, 0.5));

	/**
	 * Render the texture.
	 */
	void render() const;

private:
	/** Types of vbo. */
	enum Buffer { VERTEX, TEX_COORD, NUM_BUFFERS };

	/** The vbos. */
	GLuint buffers[NUM_BUFFERS];

	/** The vao. */
	GLuint vao;

	/** The texture buffer. */
	GLuint textureBuffer;

	/** Number of vertices to be rendered. */
	const int NUM_VERTICES;

	/** If a texture is loaded or not. */
	bool textureLoaded;

	/** Width of the texture image. */
	int width;

	/** Height of the texture image. */
	int height;

	/**
	 * Calculate vertices from a specified texture origin.
	 * @param textureOrigin The new origin of the texture.
	 * @return Pointer to the new vertices.
	 */
	GLfloat* getVerticesFromTextureOrigin(const glm::vec2& textureOrigin);

	/** Free the GL buffers. */
	void freeBuffers();
};

#endif /* TEXTURE_H_ */
