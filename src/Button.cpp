#include "Button.h"

Button::Button(const Rect& buttonRect, const glm::vec3& color):
	buttonRect(buttonRect),
	clicked(false),
	vao(0),
	NUM_VERTICES(4)
{
	generateModel(color);
}

Button::~Button()
{
	// Delete the buffers.
	if (vao != 0) {
		glDeleteVertexArrays(1, &vao);
		glDeleteBuffers(NUM_BUFFERS, buffers);
	}
}

void Button::setPosition(const glm::vec2& position)
{
	this->position = position;
}

bool Button::isWithin(const glm::vec2& position)
{
	// Calculate the button position from the position and rectangle.
	glm::vec2 buttonPos(this->position.x + buttonRect.x, this->position.y + buttonRect.y);

	// Do multiple checks to see if position is outside the button:

	if (position.x < buttonPos.x) {
		return false;
	}

	if (position.y < buttonPos.y) {
		return false;
	}

	if (position.x > buttonPos.x + buttonRect.width) {
		return false;
	}

	if (position.y > buttonPos.y + buttonRect.height) {
		return false;
	}

	return true;
}

bool Button::isClicked() const
{
	return clicked;
}

void Button::setClicked(bool state)
{
	clicked = state;
}

void Button::generateModel(const glm::vec3& color)
{
	// The sides of the button.
	GLfloat left = buttonRect.x;
	GLfloat right = buttonRect.x + buttonRect.width;
	GLfloat bottom =  buttonRect.y;
	GLfloat top = buttonRect.y + buttonRect.height;


	GLfloat buttonVertices[] = {
		left, bottom,
		left, top,
		right, bottom,
		right, top,
	};

	GLfloat buttonColors[] = {
		color.r, color.g, color.b,
		color.r, color.g, color.b,
		color.r, color.g, color.b,
		color.r, color.g, color.b,
	};

	// Generate and bind the vao.
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Generate the buffers.
	glGenBuffers(NUM_BUFFERS, buffers);

	glBindBuffer(GL_ARRAY_BUFFER, buffers[VERTEX_BUFF]);					// Bind the vertex buffer.
	glBufferData(GL_ARRAY_BUFFER, sizeof(buttonVertices), buttonVertices, GL_STATIC_DRAW);	// Send the vertex data to the buffer.
	glVertexAttribPointer((GLuint)0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);			// Set the attribute pointer to the vertex attribute.
	glEnableVertexAttribArray(0);								// Enable the vertex attribute array.

	glBindBuffer(GL_ARRAY_BUFFER, buffers[COLOR_BUFF]);					// Bind the color buffer.
	glBufferData(GL_ARRAY_BUFFER, sizeof(buttonColors), buttonColors, GL_STATIC_DRAW);	// Send the color data to the buffer.
	glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);			// Set the attribute pointer to the color attribute.
	glEnableVertexAttribArray(1);								// Enable the color attribute array.

	// Unbind the vao.
	glBindVertexArray(0);
}

void Button::render(MvpShaderProgram& shader) const
{
	// Translate to the button position.
	shader.pushTranslation(glm::vec3(position, 0.0));

	// Update the shader.
	shader.updateModelViewProjectionMatrix();

	// Render the button.
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, NUM_VERTICES);
	glBindVertexArray(0);

	// Pop the translation.
	shader.popTransformation();
}
