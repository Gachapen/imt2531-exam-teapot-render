#include "ShaderProgram.h"

#include <vector>
#include <iostream>
#include <fstream>
#include <glm/glm.hpp>

ShaderProgram::ShaderProgram():
	shaderProgram(0),
	vertexShader(0),
	fragmentShader(0)
{
}

ShaderProgram::~ShaderProgram()
{
}

bool ShaderProgram::loadShaderProgram(const std::string& vertexShaderPath, const std::string& fragmentShaderPath)
{
	// Create the shader program.
	shaderProgram = glCreateProgram();

	// If the shader program could not be created.
	if (shaderProgram == 0) {
		std::cerr << "E: Creating shader program failed: Could not create gl shader program.\n";
		return false;
	}

	// Load both shaders.
	vertexShader = loadShader(vertexShaderPath, GL_VERTEX_SHADER);
	fragmentShader = loadShader(fragmentShaderPath, GL_FRAGMENT_SHADER);

	// If one of the shaders failed to load.
	if (vertexShader == 0 || fragmentShader == 0) {
		return false;
	}

	// Attach the compiled shaders to the program.
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);

	// Link the program.
	glLinkProgram(shaderProgram);

	// Check if the linking was successful.
	if (checkLinkStatus() == false) {
		return false;
	}

	// Successfully linked the shader program.
	return true;
}

GLuint ShaderProgram::loadShader(const std::string& shaderPath, GLenum shaderType)
{
	std::string shaderString;
	std::ifstream shaderFile(shaderPath);
	GLuint shaderId;

	if (!shaderFile) {
		std::cerr << "Could not load shader file \"" << shaderPath << "\"\n";
		return false;
	}

	// Copy the shader file contents into the shader string.
	shaderString.assign(std::istreambuf_iterator<char>(shaderFile), std::istreambuf_iterator<char>());

	// Create a new shader.
	shaderId = glCreateShader(shaderType);

	// If the shader couldn't be created.
	if (shaderId == 0) {
		std::cerr << "E: Could not create shader from file \"" << shaderPath << "\"\n";
		return false;
	}

	// Set the source of the shader.
	const GLchar* shaderCString = shaderString.c_str();
	glShaderSource(shaderId, 1, &shaderCString, nullptr);

	// Compile the shader.
	glCompileShader(shaderId);

	// Check the compile status.
	if (checkCompileStatus(shaderId, shaderPath) == false) {
		return 0;
	}

	return shaderId;
}

bool ShaderProgram::checkLinkStatus()
{
	// Get the link status.
	GLint status;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &status);

	// If linking failed.
	if (status == GL_FALSE) {

		// Get the info log length.
		GLint length;
		glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &length);

		// Get the info log.
		std::vector<char> log(length);
		glGetProgramInfoLog(shaderProgram, length, &length, &log[0]);

		std::cerr << "E: Error linking shader program: " << &log[0];
		return false;
	}

	return true;
}

bool ShaderProgram::checkCompileStatus(GLuint shaderId, const std::string& shaderPath)
{
	// Get the compile status.
	GLint status;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);

	// If compilation failed.
	if (status == GL_FALSE) {

		// Get the info log length.
		GLint length;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &length);

		// Get the info log.
		std::vector<char> log(length);
		glGetShaderInfoLog(shaderId, length, &length, &log[0]);

		std::cerr << "E: Shader compilation error: " << shaderPath << ": " << &log[0];
		return false;
	}

	return true;
}

bool ShaderProgram::initUniform(const std::string& name)
{
	// Shader program not created yet.
	if (shaderProgram == 0) {
		std::cerr << "E: Trying to find a uniform in a shader program that doesn't exist.\n" << std::endl;
		return false;
	}

	// Get the uniform location.
	GLint uniformId = glGetUniformLocation(shaderProgram, name.c_str());

	// If the uniform was not found.
	if (uniformId == -1) {
		std::cerr << "E: The uniform " << name << " could not be found in shader program " << shaderProgram << std::endl;
		return false;
	}

	// Add the uniform location to the map.
	uniforms[name] = uniformId;
	return true;
}

GLint ShaderProgram::getUniformLocation(const std::string& name)
{
	// If not found and not able to initialize, return 0.
	if (uniforms.find(name) == uniforms.end() && initUniform(name) == false) {
		return 0;
	}

	// Return the uniform location.
	return uniforms[name];
}

void ShaderProgram::enable()
{
	// Use this shader program.
	if (shaderProgram != 0) {
		glUseProgram(shaderProgram);
	} else {
		std::cerr << "E: Trying to enable a shader program that does not exist.\n";
	}
}
