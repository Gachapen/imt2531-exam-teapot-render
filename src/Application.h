#ifndef APPLICATION_H_
#define APPLICATION_H_

#include <stack>
#include <glm/glm.hpp>
#include "RenderApp.h"
#include "ShaderProgram.h"
#include "MvpShaderProgram.h"
#include "Object.h"
#include "gl_states.h"
#include "ColorController.h"
#include "Texture.h"
#include "Camera.h"

/**
 * The application that will render our 3D world.
 */
class Application: public RenderApp {
public:
	Application(unsigned int windowWidth, unsigned int windowHeight, unsigned int windowBpp, const std::string& windowTitle);
	virtual ~Application();

private:
	/** The shader used for 3D rendering. */
	MvpShaderProgram phongShader;

	/** Shader used for rendering the controls. */
	MvpShaderProgram orthoShader;

	/** Shader used to render textures. */
	MvpShaderProgram textureShader;

	/** The background texture. */
	Texture* backgroundTexture;

	/** The 3D model to render. */
	Object* model;

	/** The controller used to control the material colors. */
	ColorController* colorController;

	/** The 3D lightning. */
	Light light;

	/** The 3D material. */
	Material material;

	/** Controls the 3D view. */
	Camera camera;

	bool initGl();

	void update();
	void render();

	void onResize(unsigned int width, unsigned int height);
	void onKeyDown(const SDL_keysym& keysym);
	void onMouseMotion(const SDL_MouseMotionEvent& mouseMotion);
	void onEvent(const SDL_Event& event);
};

#endif /* APPLICATION_H_ */
