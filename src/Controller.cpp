#include "Controller.h"

#include <iostream>

Controller::Controller()
{
}

Controller::~Controller()
{
	// Delete all sliders.
	for (auto sliderIt = sliders.begin(); sliderIt != sliders.end(); sliderIt++) {
		delete (*sliderIt).second;
	}
}

void Controller::addSlider(const std::string& name, Slider* slider)
{
	// Check for errors and add slider if it's OK.
	if (slider == nullptr) {
		std::cerr << "E: Trying to add nullptr slider to controller.\n";
	} else if (sliders.find(name) != sliders.end()) {
		std::cerr << "E: Slider with the name " << name << " already exists in controller.\n";
	} else {
		sliders[name] = slider;
	}
}

float Controller::getSliderValue(const std::string& sliderName)
{
	// Check if the slider exists, and if so, return the sliders value.
	if (sliders.find(sliderName) == sliders.end()) {
		std::cerr << "E: Trying to get value from non-existing slider \"" << sliderName << "\" in controller.\n";
		return 0.0f; // Slider not found.
	} else {
		return sliders[sliderName]->getValue();
	}
}

void Controller::render(MvpShaderProgram& shader) const
{
	// Render all sliders.
	for (auto sliderIt = sliders.begin(); sliderIt != sliders.end(); sliderIt++) {
		(*sliderIt).second->render(shader);
	}
}

void Controller::onEvent(const SDL_Event& event)
{
	// Check event against all sliders.
	for (auto sliderIt = sliders.begin(); sliderIt != sliders.end(); sliderIt++) {
		(*sliderIt).second->onEvent(event);
	}
}
