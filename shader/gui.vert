#version 420

// The vertex and color attibutes.
layout(location = 0) in vec2 vertex;
layout(location = 1) in vec3 color;

uniform mat4 modelViewProjectionMatrix;

// The color to send to the fragment shader.
out vec3 fragColor;

void main() {
	// Set the color.
	fragColor = color;
	
	// Calculate the vertex position.
	gl_Position = modelViewProjectionMatrix * vec4(vertex, 1.0, 1.0);
}
