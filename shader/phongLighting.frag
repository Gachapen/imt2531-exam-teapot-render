#version 420

struct MyMaterial
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

uniform MyMaterial material;

struct MyLight
{
    vec4 position;  //Set w=0 for direction light, or w=1 for point light
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform MyLight light;

in vec3 N;
in vec3 v;

out vec4 outputFragColor;

void main() {
   // v current is the 3D position of the current point on the surface of the triangle
   vec3 L = normalize(light.position.xyz - v);   // A vector pointing toward the light (for a point light source)
   vec3 E = normalize(-v); // A vector pointing toward the eye/camera. We are in Eye Coordinates, so EyePos is (0,0,0)  
   vec3 R = normalize(-reflect(L,N));  // The L reflected in the surface normal, N
 
   // Calculate Ambient Term:
   vec3 Iamb = material.ambient * light.ambient;
   
   // Calculate Diffuse Term:  
   vec3 Idiff = material.diffuse * light.diffuse * max(dot(N,L), 0.0);
   
   // Calculate Specular Term:
   vec3 Ispec = material.specular * light.specular
                * pow(max(dot(R,E),0.0),0.3*material.shininess);
   Ispec = clamp(Ispec, 0.0, 1.0); 

   // write Total Color:
   outputFragColor = vec4((Iamb + Idiff) + Ispec, 1);
}
