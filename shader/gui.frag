#version 420

// The color from the vertex shader.
in vec3 fragColor;

// The final fragment color.
out vec4 outputFragColor;

void main() {
	// Set the fragment color to the color from the verte shader.
	outputFragColor = vec4(fragColor, 1.0);
}
