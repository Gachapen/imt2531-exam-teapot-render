#version 420

// The vertex and texture coordinate attributes,
layout(location = 0) in vec2 vertex;
layout(location = 1) in vec2 texCoord;

uniform mat4 modelViewProjectionMatrix;

// The color to send to the fragment shader.
out vec2 fragTexCoord;

void main() {
	// Set the color.
	fragTexCoord = texCoord;

	// Calculate the vertex position.
	gl_Position = modelViewProjectionMatrix * vec4(vertex, 1.0, 1.0);
}
