#version 420

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;

uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat3 normalMatrix;

out vec3 N;
out vec3 v;

void main() {
   v = vec3(modelViewMatrix * vec4(vertex, 1));   // Calc the vertex in camera space. (Still in 3D)
   N = normalize(normalMatrix * normal); // Calc the normal in camera space. Note, this deals with non-uniform scales correctly
 
   gl_Position = modelViewProjectionMatrix * vec4(vertex, 1);
}
