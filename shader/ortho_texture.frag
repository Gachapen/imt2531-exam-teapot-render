#version 420

// The color from the vertex shader.
in vec2 fragTexCoord;

// The texture sampler.
layout(binding = 0) uniform sampler2D theTexture;

// The final fragment color.
out vec4 outputFragColor;

void main() {
    // Get the texture color at the current texture coordinate.
    vec3 texCol = texture(theTexture, fragTexCoord).rgb;

    // Set the fragment color to the texture color.
    outputFragColor = vec4(texCol, 1);
}
